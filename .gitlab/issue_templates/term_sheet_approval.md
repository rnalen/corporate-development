## Project [project_code_name] Term Sheet Approvals

** Please DO NOT include any deal specific confidential details such as payment terms, milestones and more **

* Main collaboration document [insert_hyperlink]
* Term sheet [insert_hyperlink] - please refer to this document for the deal terms.

The purpose of this issue is to track approvals to negotiate a term sheet.

## Approval of term sheet to start negotiations

* [ ]  CEO
* [ ]  CFO
* [ ]  CLO
* [ ]  Chief Product Officer 
    * [ ]  Product Champion
* [ ]  CTO
    * [ ]  Engineering Champion
* [ ]  Dir. Corp Dev

## Final approval of term sheet for signing
- Link to changes to term sheet document [slack message, GitLab comment or gdoc section]
* [ ]  Chief Product Officer 
* [ ]  CLO
* [ ]  CFO
* [ ]  CEO
