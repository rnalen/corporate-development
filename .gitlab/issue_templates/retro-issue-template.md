This is an asynchronous retrospective for Project --[ADD NAME]--.

This issue is private (confidential) as we track the information shared here. Please avoid from disclosing deal-specific commercial or personal details. The retrospective in this issue will conclude on --[ADD DATE]-- and will likely be followed by an _optional_ sync call. This issue may be open to the public at a later stage pending a review.

Please look at back at your experiences working through this acquisition, ask yourself
**:star2: what praise do you have for the group?**,
**:+1: what went well?**, **:-1: what didn’t go well?**, and **:chart_with_upwards_trend: what can we improve going
forward?**, and honestly describe your thoughts and feelings below. You can relate to the phases of the acquisition as a reference: exploratory, early diligence, diligence. 

For each point you want to raise, please create a **new discussion** with the
relevant emoji, so that others can weigh in with their perspectives, and so that
we can easily discuss any follow-up action items in-line.

If there is anything you are not comfortable sharing here, feel free to message @eliran.mesika directly. Note, however, that 'Emotions are not only allowed in
retrospectives, they should be encouraged', so we'd love to hear from you here
if possible.
